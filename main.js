// let app = new Vue ({
//     el: '#app',
//     data: {
//         str: 'Hello vue',
//         bindClass: 'bind',
//         stateIf: true,
//         stateElseIf: false,
//         stateShow: false,
//         stateLoop: [
//             'Vue',
//             'React',
//             'Angular',
//             'Ember'
//         ],
//         // v-bind
//         img: {
//             src: 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjmk4Cu6PfgAhUYwcQBHbxtCeEQjRx6BAgBEAU&url=https%3A%2F%2Ftwitter.com%2Fvuenewsletter&psig=AOvVaw0NRNXchsGdzMgkHD6m1bEC&ust=1552315588253751',
//             alt: 'Vue.js course'
//         },
//         dataAttr: true,
//         toggler: true
//     }
// })

// let app2 = new Vue({
//     el: '#classWork',
//     data: {
//         arr: [
//             'str 1',
//             'str 2'
//         ]
//     }
// });

let app = new Vue({
    el: '#app',
    data: {
        test: 'test',
        title: 'Dynamo',
        teams: [
            {
                title: 'Dynamo',
                class: 'Dynamo',
                bestPlayers: [
                    'Zygankov',
                    'Mikolenko',
                    'Verbich',
                ]
            },
            {
                title: 'Arsenal',
                class: 'Arsenal',
                bestPlayers: [
                    'Aubameyang',
                    'Lacazette',
                    'Xhaka',
                ]
            },
            {
                title: 'Barcelona',
                class: 'Barcelona',
                bestPlayers: [
                    'Messi',
                    'Grisman',
                    'Suares',
                ]
            }
        ]
    }
});